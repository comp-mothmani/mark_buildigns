package com.qosdesign.markbuilding.ccmp;

public class CMenuItem {

    private CMenuItemClickListener event;
    private CMenuItemTypes type ;
    private int grpId ;
    private int itmId ;
    private int order ;
    private String title ;
    private int icon ;

    public CMenuItem(int itmId){
        this.itmId = itmId ;
    }

    public CMenuItem(){

    }

    public CMenuItem(CMenuItemTypes type, int itmId, String title, CMenuItemClickListener event) {
        this.type = type;
        this.itmId = itmId;
        this.title = title;
        this.event = event ;
    }

    public void action(){
        event.clickEvent(this);
    }




    public void attach(CActivity activity){
        if(type == CMenuItemTypes.SubMenu){
            activity.addSubMenu(title);
        }else if(type == CMenuItemTypes.HeaderMenuItem){
            activity.addMenuItem(getWrapper());
        }else if(type == CMenuItemTypes.BottomMenuItem){
            activity.addBottomMenuItem(getWrapper());
        }
    }
    public MenuItemWrapper getWrapper(){
        return new MenuItemWrapper(grpId, itmId, order, title, icon) ;
    }

    public CMenuItemTypes getType() {
        return type;
    }

    public void setType(CMenuItemTypes type) {
        this.type = type;
    }

    public int getGrpId() {
        return grpId;
    }

    public void setGrpId(int grpId) {
        this.grpId = grpId;
    }

    public int getItmId() {
        return itmId;
    }

    public void setItmId(int itmId) {
        this.itmId = itmId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
