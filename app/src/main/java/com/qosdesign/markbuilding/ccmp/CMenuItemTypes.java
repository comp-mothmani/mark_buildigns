package com.qosdesign.markbuilding.ccmp;

public enum CMenuItemTypes {
    SubMenu,
    HeaderMenuItem,
    BottomMenuItem,
}
