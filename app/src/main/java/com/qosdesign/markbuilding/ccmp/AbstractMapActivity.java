package com.qosdesign.markbuilding.ccmp;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.qosdesign.markbuilding.MBMapsActivity;
import com.qosdesign.markbuilding.R;
import com.qosdesign.markbuilding.data.pojo.Building;
import com.qosdesign.markbuilding.data.pojo.BuildingsList;
import com.qosdesign.markbuilding.utils.Circles;
import com.qosdesign.markbuilding.utils.Markers;
import com.qosdesign.markbuilding.utils.PermissionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;

public abstract class AbstractMapActivity extends CActivity implements OnMapReadyCallback, GoogleMap.OnCameraChangeListener, GoogleMap.OnMapClickListener {

    protected  BuildingsList buildings_list;
    protected  GoogleMap mMap;
    protected boolean clicked = false;
    protected static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    protected int mStyleIds[] = {
            R.string.style_label_Hybrid,
            R.string.style_label_Normal,
            R.string.style_label_retro,
            R.string.style_label_night,
            R.string.style_label_dark,
            R.string.style_label_aubergine,
            R.string.style_label_grayscale, +
            R.string.style_label_no_pois_no_transit,
            R.string.style_label_default,
    };
    protected int mSelectedStyleId = R.string.style_label_default;
    protected static final String SELECTED_STYLE = "selected_style";
    protected TextView map_type_txt;
    protected NavigationView nv;
    protected DrawerLayout dl;
    protected ActionBarDrawerToggle t;
    protected String restoredText;
    protected ArrayList<MBMapsActivity.DataModel> modelArrayList = new ArrayList<>();
    protected SparseBooleanArray checked = new SparseBooleanArray();
    protected ArrayList<Building> selectedItems_list = new ArrayList<Building>();
    protected Markers markers;
    protected ArrayList<MBMapsActivity.DataModel> checkedItems = new ArrayList<>();
    protected HashMap<Marker,Building> hashmap = new HashMap<>();
    protected Circle currentCircle;
    protected Circles circles;
    protected List<String> building_type_array = new ArrayList<>();
    private static final int REQUEST_CODE_GOOGLE_PLAY_SERVECES_ERROR = -1;
    private static final double EARTH_RADIOUS = 3958.75; // Earth radius;
    private static final int METER_CONVERSION = 1609;
    protected Activity activity;


    @Override
    public String actionBarColor() {
        return "#0277BD";
    }

    @Override
    public void addActionBarButton() {
    }

    @Override
    public int setOptionsMenuView() {
        return R.menu.custom_menu;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         activity = this;

        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        init();

        buildings_list = new BuildingsList();

        loadOfflineData();

        /***
         * init drawer layout, put it inside a method
         */
        t = new ActionBarDrawerToggle(this, dl, R.string.open, R.string.close);
        dl.addDrawerListener(t);
        dl.setScrimColor(Color.TRANSPARENT);
        t.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ///int  parameters of building type spinner
        building_type_array.add("MDU");
        building_type_array.add("SDU");
    }

    public void loadOfflineData(){
        ///--- load data from shared pref
        SharedPreferences prefs = getSharedPreferences("WO_MARKED_BUILDINGS", MODE_PRIVATE);
        restoredText = prefs.getString("buildings", null);

        if (restoredText != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                buildings_list = mapper.readValue(restoredText, BuildingsList.class);

                ///--- init list view
                final List<String> items = new ArrayList<>();
                for (Building tmp : buildings_list.getBuildings()) {

                    MBMapsActivity.DataModel model = new MBMapsActivity.DataModel();
                    model.setSelected(false);
                    model.setBuilding_name(tmp.getName());
                    modelArrayList.add(model);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        /****
         * set min zoom level to 17, if we return from one of the principal menu activities.
         * if there is intent, or set the usual zoom level 7
         */

        final LatLng tunis = new LatLng(36.84262034970311, 10.16230225928363);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(tunis, 14));
        markers = new Markers();
        circles = new Circles();

        drawBuildings();

        mMap.setOnCameraChangeListener(this);
       //mMap.setOnMapClickListener(this);


    }

    public void showStylesDialog() {
        // mStyleIds stores each style's resource ID, and we extract the names here, rather
        // than using an XML array resource which AlertDialog.Builder.setItems() can also
        // accept. We do this since using an array resource would mean we would not have
        // constant values we can switch/case on, when choosing which style to apply.
        List<String> styleNames = new ArrayList<>();
        for (int style : mStyleIds) {
            styleNames.add(getString(style));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.style_choose));
        builder.setItems(styleNames.toArray(new CharSequence[styleNames.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mSelectedStyleId = mStyleIds[which];
                        String msg = getString(R.string.style_set_to, getString(mSelectedStyleId));
                        map_type_txt.setText("Map Type: "+ getString(mSelectedStyleId));
                        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
                        setSelectedStyle();
                    }
                });
        builder.show();
    }

    private void setSelectedStyle() {
        MapStyleOptions style = null;
        switch (mSelectedStyleId) {
            case R.string.style_label_Hybrid:
                // Sets the retro style via raw resource JSON.
                mMap.setMapType(MAP_TYPE_HYBRID);
                break;

            case R.string.style_label_Normal:
                // Sets the retro style via raw resource JSON.
                mMap.setMapType(MAP_TYPE_NORMAL);
                break;

            case R.string.style_label_retro:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the retro style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_retro);
                }
                break;

            case R.string.style_label_night:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the night style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_night);
                }
                break;

            case R.string.style_label_dark:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the night style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.dark);
                }
                break;

            case R.string.style_label_aubergine:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the night style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.aubergine);
                }
                break;

            case R.string.style_label_grayscale:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the grayscale style via raw resource JSON.
                    style = MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_grayscale);
                }
                break;

            case R.string.style_label_no_pois_no_transit:
                if (mMap.getMapType() == MAP_TYPE_HYBRID) {
                    Toast.makeText(getApplicationContext(), "You cannot overcome Hybrid layer, please change map type to Normal layer, to choose other map types", Toast.LENGTH_LONG).show();
                } else {
                    // Sets the no POIs or transit style via JSON string.
                    style = new MapStyleOptions("[" +
                            "  {" +
                            "    \"featureType\":\"poi.business\"," +
                            "    \"elementType\":\"all\"," +
                            "    \"stylers\":[" +
                            "      {" +
                            "        \"visibility\":\"off\"" +
                            "      }" +
                            "    ]" +
                            "  }," +
                            "  {" +
                            "    \"featureType\":\"transit\"," +
                            "    \"elementType\":\"all\"," +
                            "    \"stylers\":[" +
                            "      {" +
                            "        \"visibility\":\"off\"" +
                            "      }" +
                            "    ]" +
                            "  }" +
                            "]");
                }

                break;
            case R.string.style_label_default:
                // Removes previously set style, by setting it to null.
                style = null;
                break;
            default:
                return;
        }
        mMap.setMapStyle(style);
    }

    public boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(this, "Map is not ready yet", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // Store the selected map style, so we can assign it when the activity resumes.
        outState.putInt(SELECTED_STYLE, mSelectedStyleId);
        super.onSaveInstanceState(outState);
    }

    private void updateMyLocation() {
        if (!checkReady()) {
            return;
        }
        // Enable the location layer. Request the location permission if needed.
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            // Uncheck the box until the layer has been enabled and request missing permission.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, false);
        }
    }

    public void initLocationActionBarItem() {
        if (clicked == false) {
            updateMyLocation();
            clicked = true;
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(false);
            clicked = false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        /***
         * Navigatin view
         */
        if (t.onOptionsItemSelected(item)) {
            return true;

        } else super.onOptionsItemSelected(item);

        return true;
    }

    public void drawBuildings(){
        for (Building tmp : buildings_list.getBuildings()){
            int heightO = 50;
            int widthO = 35;
            BitmapDrawable bitmapdrawcs = (BitmapDrawable) getResources().getDrawable(R.drawable.buildings);
            Bitmap bcs = bitmapdrawcs.getBitmap();
            Bitmap csicon = Bitmap.createScaledBitmap(bcs, widthO, heightO, false);

            Marker markerOlt = mMap.addMarker(new MarkerOptions()
                    .position(tmp.position()).title(tmp.getName())
                    .icon(BitmapDescriptorFactory.fromBitmap(csicon)));

            markers.put(markerOlt, tmp);
        }
    }
    public void drawBuilding(Building tmp){
        int heightO = 50;
        int widthO = 35;
        BitmapDrawable bitmapdrawcs = (BitmapDrawable) getResources().getDrawable(R.drawable.buildings);
        Bitmap bcs = bitmapdrawcs.getBitmap();
        Bitmap csicon = Bitmap.createScaledBitmap(bcs, widthO, heightO, false);

        Marker markerOlt = mMap.addMarker(new MarkerOptions()
                .position(tmp.position()).title(tmp.getName())
                .icon(BitmapDescriptorFactory.fromBitmap(csicon)));

        markers.put(markerOlt, tmp);

    }

    public void createCircle(LatLng currentMarker, Double radius) {

        //check circle is exist or not
        //if exist remove
        if (currentCircle != null) {
            currentCircle.remove();
        }
        currentCircle = mMap.addCircle(new CircleOptions().center(currentMarker).radius(radius)
                .strokeColor(Color.parseColor("#FF007A93"))
                .fillColor(Color.parseColor("#40007A93"))
                .strokeWidth(4));

        circles.put(currentCircle, currentMarker);

    }

    public void titlMap(){

        LatLng center = mMap.getCameraPosition().target;
        float zm = mMap.getCameraPosition().zoom;
        LatLng dist = new LatLng(center.latitude + 0.00001, center.longitude + 0.00001);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dist, (float) (zm + 0.1)));
        zm = mMap.getCameraPosition().zoom;
        center = mMap.getCameraPosition().target;
        dist = new LatLng(center.latitude - 0.00001, center.longitude - 0.00001);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dist, (float) (zm - 0.1)));
    }


}
