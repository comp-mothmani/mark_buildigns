package com.qosdesign.markbuilding.ccmp;

public interface CMenuItemClickListener {

    public void clickEvent(CMenuItem it) ;
}
