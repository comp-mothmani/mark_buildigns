package com.qosdesign.markbuilding.ccmp;

public class MenuItemWrapper{

    private int grpId ;
    private int itmId ;
    private int order ;
    private String title ;
    private int icon ;

    public MenuItemWrapper() {

    }

    public MenuItemWrapper(int grpId, int itmId, int order, String title, int icon) {
        this.grpId = grpId;
        this.itmId = itmId;
        this.order = order;
        this.title = title;
        this.icon = icon;
    }

    public int getGrpId() {
        return grpId;
    }

    public void setGrpId(int grpId) {
        this.grpId = grpId;
    }

    public int getItmId() {
        return itmId;
    }

    public void setItmId(int itmId) {
        this.itmId = itmId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
