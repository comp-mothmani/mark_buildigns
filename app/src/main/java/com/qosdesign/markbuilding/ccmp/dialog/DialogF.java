package com.qosdesign.markbuilding.ccmp.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.qosdesign.markbuilding.R;

public  class DialogF extends CDialog {

        private static View view;
        private DialogBack back;
        public EditText txt ;

        public DialogF newInstance(int title,DialogBack back) {
            this.back = back ;
            DialogF instance = new DialogF();
            setStyle(DialogFragment.STYLE_NO_TITLE, 0);
            Bundle args = new Bundle();
            args.putInt("title", title);
            instance.setArguments(args);
            return instance;

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 final Bundle savedInstanceState) {

            if (view != null) {
                ViewGroup parent = (ViewGroup) view.getParent();
                if (parent != null)
                    parent.removeView(view);

                ////---dialog style---
                setCancelable(false);
            }
            view = inflater.inflate(setLayoutView(), container, false);

            return view;

        }

    @Override
    public void initViews() {

    }

    @Override
    public void fill() {

    }

    @Override
    public void reverseFill() {

    }

    @Override
    public int setLayoutView() {
        return R.layout.add_new_building_layout;
    }
}