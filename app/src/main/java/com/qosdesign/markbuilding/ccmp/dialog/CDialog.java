package com.qosdesign.markbuilding.ccmp.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public abstract class CDialog extends DialogFragment {

    public abstract int setLayoutView();
    protected View view;
    private DialogBack back;
//    protected BottomNavigationView btm;
    public void init(LayoutInflater inflater, ViewGroup container,
                     final Bundle savedInstanceState){

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);

            setCancelable(false);
        }

        view = inflater.inflate(setLayoutView(), container, false);
        setCancelable(false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);

            setCancelable(false);
        }
        view = inflater.inflate(setLayoutView(), container, false);

        initViews();

        fill();

        reverseFill();

        setCancelable(false);


        return view;
    }


    public abstract void initViews() ;
    public abstract void fill() ;
    public abstract void reverseFill() ;

}
