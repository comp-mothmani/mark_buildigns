package com.qosdesign.markbuilding.ccmp;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.qosdesign.markbuilding.R;

import java.util.ArrayList;
import java.util.List;

public abstract class CActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    protected CMenuItems items = new CMenuItems() ;
    protected List<String> subMenus = new ArrayList<>() ;
    protected List<MenuItemWrapper> menuItemWrappers = new ArrayList<>() ;
    protected List<MenuItemWrapper> menuItemBottomWrappers = new ArrayList<>() ;
    protected BottomNavigationView btmNvVw ;

    public CActivity(){

    }

    public void init(){
        getSupportActionBar().setTitle("" + actionBarTitle());
        getSupportActionBar().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor(actionBarColor()))
        );
        initData();
        initViews();
    }

    @SuppressLint("NewApi")
    public void initBottomNavigationMenu(){
        btmNvVw = findViewById(R.id.btmNavViw) ;
        btmNvVw.setBackground(new ColorDrawable(Color.parseColor(actionBarColor())));
        for(MenuItemWrapper tmp : menuItemBottomWrappers){
            MenuItem mnn = btmNvVw.getMenu().add(tmp.getGrpId(), tmp.getItmId(), tmp.getOrder(), tmp.getTitle());
            mnn.setIcon(tmp.getIcon()) ;
            mnn.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        btmNvVw.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        for(CMenuItem tmp : items.filterByType(CMenuItemTypes.BottomMenuItem)){
            if(tmp.getTitle().equals(menuItem.getTitle())){
                tmp.action();
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem it){


        if(!it.isActionViewExpanded()){
            for(CMenuItem tmp : items.filterByType(CMenuItemTypes.SubMenu)){
                if(tmp.getTitle().equals(it.getTitle())){
                    tmp.action();
                }
            }
        }
        for(CMenuItem tmp : items.filterByType(CMenuItemTypes.HeaderMenuItem)){
            if(tmp.getTitle() == it.getTitle()){
                tmp.action();
            }
        }

        return true ;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu mn){
        getMenuInflater().inflate(setOptionsMenuView(),mn);
        mn.clear();

        for(MenuItemWrapper tmp : menuItemWrappers){
            MenuItem mnn = mn.add(tmp.getGrpId(), tmp.getItmId(), tmp.getOrder(), tmp.getTitle());
            mnn.setIcon(tmp.getIcon()) ;
            mnn.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        for(String tmp : subMenus){
            mn.addSubMenu(tmp);
        }
        return true;
    }

    public void addSubMenu(String ss){
        subMenus.add(ss) ;
    }

    public void addMenuItem(MenuItemWrapper wrapper){
        menuItemWrappers.add(wrapper) ;
    }

    public void addBottomMenuItem(MenuItemWrapper wrapper){
        menuItemBottomWrappers.add(wrapper) ;
    }

    public abstract void initData() ;
    public abstract void initViews() ;
    public abstract String actionBarTitle() ;
    public abstract String actionBarColor() ;
    public abstract void addActionBarButton() ;
    public abstract int setOptionsMenuView() ;

}
