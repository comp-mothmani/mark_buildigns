package com.qosdesign.markbuilding.ccmp.dialog;


public interface DialogBack {
    public void back(CDialog dialogF) ;
    public void save(CDialog dialogF) ;
}