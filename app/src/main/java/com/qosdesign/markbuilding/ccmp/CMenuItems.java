package com.qosdesign.markbuilding.ccmp;

import java.util.ArrayList;

public class CMenuItems extends ArrayList<CMenuItem> {

    public CMenuItems(){

    }

    public CMenuItems filterByType(CMenuItemTypes tp){
        CMenuItems tmps = new CMenuItems() ;
        for(CMenuItem tmp : this){
            if(tmp.getType() == tp){
                tmps.add(tmp) ;
            }
        }

        return tmps ;
    }
}
