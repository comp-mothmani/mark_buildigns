package com.qosdesign.markbuilding.utils;

import com.google.android.gms.maps.model.Circle;
import com.qosdesign.markbuilding.data.pojo.Building;

import java.util.HashMap;

public class Circles  extends HashMap<Circle,Object> {

    public Circles deleteCircles() {
        for (Circle circle : this.keySet()) {
            if (this.get(circle) instanceof Building) {
                circle.remove();
            }
        }
        return null;
    }
}
