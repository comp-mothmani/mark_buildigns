package com.qosdesign.markbuilding.utils.generator;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.qosdesign.markbuilding.AppContext;
import com.qosdesign.markbuilding.data.collection.AppartmentC;
import com.qosdesign.markbuilding.data.pojo.Appartment;
import com.qosdesign.markbuilding.data.pojo.Block;
import com.qosdesign.markbuilding.data.pojo.Building;
import com.qosdesign.markbuilding.data.pojo.BuildingsList;
import com.qosdesign.markbuilding.data.pojo.Floor;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GenTest {

    private Activity activity;
    private String nameFile;
    private BuildingsList buildingsList;

    public GenTest(Activity activity) {
     this.activity = activity;
    }

    public HSSFWorkbook render()  {

        buildingsList = new BuildingsList();
        buildingsList = AppContext.BUILDINGS_LIST;

        List<Appartment> appartments_list = new ArrayList<>();
        for (Building tmp : buildingsList.getBuildings()) {
            for (Block tmp1 : tmp.getBlocks()) {
                for (Floor tmp2: tmp1.getFloors()){
                    for (Appartment tmp3: tmp2.getAppartmens()){
                        appartments_list.add(tmp3);
                    }
                }
            }
        }

        HSSFWorkbook wb = new HSSFWorkbook();

        int outlets_cmp = 0;

        HSSFSheet sheet1 = wb.createSheet("list");

        //Cell Style for alignment and border
        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cellStyle.setLocked(true);
        cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
        cellStyle.setBorderRight(CellStyle.BORDER_THIN);
        cellStyle.setBorderTop(CellStyle.BORDER_THIN);

        //Cell Style for Headers
        CellStyle style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setLocked(true);
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setBorderTop(CellStyle.BORDER_THIN);
        Font font = wb.createFont();
        font.setFontHeightInPoints((short) 8);
        font.setFontName("Arial");
        font.setColor(IndexedColors.DARK_BLUE.getIndex());
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style.setFont(font);

        //Cell Style for SubHeaders
        CellStyle style2 = wb.createCellStyle();
        style2.setAlignment(CellStyle.ALIGN_CENTER);
        style2.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style2.setLocked(true);
        style2.setBorderBottom(CellStyle.BORDER_THIN);
        style2.setBorderLeft(CellStyle.BORDER_THIN);
        style2.setBorderRight(CellStyle.BORDER_THIN);
        style2.setBorderTop(CellStyle.BORDER_THIN);
        Font font2 = wb.createFont();
        font2.setFontHeightInPoints((short) 8);
        font2.setFontName("Arial");
        font2.setColor(IndexedColors.BLACK.getIndex());
        font2.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style2.setFont(font2);

        //Cell Style for Close cell BottappartmentsPerFloorPerBloc.put(bloc, newHash);om Border
        CellStyle styleBorder = wb.createCellStyle();
        styleBorder.setBorderBottom(CellStyle.BORDER_THIN);


        Row row1 = sheet1.createRow(0);
        HSSFCell cellR1_0 = (HSSFCell) row1.createCell(0);
        cellR1_0.setCellValue("Zone name");
        cellR1_0.setCellStyle(style);
        HSSFCell cellR1_1 = (HSSFCell) row1.createCell(1);
        cellR1_1.setCellValue("Zone code");
        cellR1_1.setCellStyle(style);
        HSSFCell cellR1_2 = (HSSFCell) row1.createCell(2);
        cellR1_2.setCellValue("Outlets");
        cellR1_2.setCellStyle(style);

        Row row2 = sheet1.createRow(1);
        HSSFCell cellR2_0 = (HSSFCell) row2.createCell(0);
        cellR2_0.setCellValue("zone1");
        cellR2_0.setCellStyle(style);
        HSSFCell cellR2_1 = (HSSFCell) row2.createCell(1);
        cellR2_1.setCellValue("1111");//TODO proposer au user de sairir un code pour la zone avant de l'exporter (facultatif)
        cellR2_1.setCellStyle(style);
        HSSFCell cellR2_2 = (HSSFCell) row2.createCell(2);
        //cellR2_2.setCellValue(0);//mise a jour plus loin ( a la fin)
        cellR2_2.setCellStyle(style);


        Row row3 = sheet1.createRow(2);
        Row row4 = sheet1.createRow(3);

        HSSFCell cell0 = (HSSFCell) row3.createCell(0);
        cell0.setCellValue("ID");
        cell0.setCellStyle(style);
        HSSFCell cell1 = (HSSFCell) row3.createCell(1);
        cell1.setCellValue("Nom");
        cell1.setCellStyle(style);
        HSSFCell cell2 = (HSSFCell) row3.createCell(2);
        cell2.setCellValue("Les coordonnées");
        cell2.setCellStyle(style);
        HSSFCell cell21 = (HSSFCell) row4.createCell(2);
        cell21.setCellValue("Longitude");
        cell21.setCellStyle(style2);
        HSSFCell cell22 = (HSSFCell) row4.createCell(3);
        cell22.setCellValue("Latitude");
        cell22.setCellStyle(style2);
        HSSFCell cell3 = (HSSFCell) row3.createCell(4);
        cell3.setCellValue("Adresse");
        cell3.setCellStyle(style);
        HSSFCell cell4 = (HSSFCell) row3.createCell(5);
        cell4.setCellValue("Type");
        cell4.setCellStyle(style);
        HSSFCell cell5 = (HSSFCell) row3.createCell(6);
        cell5.setCellValue("Nombre de Blocs");
        cell5.setCellStyle(style);
        HSSFCell cell6 = (HSSFCell) row3.createCell(7);
        cell6.setCellValue("Nombre d'apparts par étage par bloc");
        cell6.setCellStyle(style);
        HSSFCell cell61 = (HSSFCell) row4.createCell(7);
        cell61.setCellValue("Bloc");
        cell61.setCellStyle(style2);
        HSSFCell cell62 = (HSSFCell) row4.createCell(8);
        cell62.setCellValue("Nombre étages");
        cell62.setCellStyle(style2);
        HSSFCell cell63 = (HSSFCell) row4.createCell(9);
        cell63.setCellValue("Résidence");
        cell63.setCellStyle(style2);
        HSSFCell cell64 = (HSSFCell) row4.createCell(10);
        cell64.setCellValue("Business");
        cell64.setCellStyle(style2);
        HSSFCell cell7 = (HSSFCell) row3.createCell(11);
        cell7.setCellValue("Nombre habitants");
        cell7.setCellStyle(style);

        for (int i = 0; i <= 11; i++) {
            if ((i != 2) && (i != 3) && (i != 7) && (i != 8) && (i != 9) && (i != 10)) {
                sheet1.addMergedRegion(new CellRangeAddress(
                        2, //first row (0-based)
                        3, //last row  (0-based)
                        i, //first column (0-based)
                        i  //last column  (0-based)
                ));
            }
            if (i == 2) {
                sheet1.addMergedRegion(new CellRangeAddress(
                        2, //first row (0-based)
                        2, //last row  (0-based)
                        i, //first column (0-based)
                        i + 1  //last column  (0-based)
                ));
            }
            if (i == 7) {
                sheet1.addMergedRegion(new CellRangeAddress(
                        2, //first row (0-based)
                        2, //last row  (0-based)
                        i, //first column (0-based)
                        i + 3  //last column  (0-based)
                ));
            }
        }

//        sheet1.autoSizeColumn(0);
//        sheet1.autoSizeColumn(1);
//        sheet1.autoSizeColumn(2);
//        sheet1.autoSizeColumn(3);
//        sheet1.autoSizeColumn(4);
//        sheet1.autoSizeColumn(5);
//        sheet1.autoSizeColumn(6);
//        sheet1.autoSizeColumn(7);
//        sheet1.autoSizeColumn(8);
//        sheet1.autoSizeColumn(9);
//        sheet1.autoSizeColumn(10);
//        sheet1.autoSizeColumn(11);

        //Remplir les Lignes de Building List

        //Remplir le tableau de la liste créée
        int l = 4;//commencer par le row d index 4
        int a = 4;
        int m = 4;


        if(!buildingsList.getBuildings().isEmpty()){
            int j = 0;
            for(Building tmp : buildingsList.getBuildings()){

                if (tmp.getType().equals("SDU")){

                    HSSFRow row = sheet1.createRow(l);
                    HSSFCell cell00 = (HSSFCell)row.createCell(0);
                    //cell00.setCellValue(buildingList.get(j).getId());
                    cell00.setCellValue(j); //building id
                    cell00.setCellStyle(cellStyle);
                    HSSFCell cell01 = (HSSFCell)row.createCell(1);
                    cell01.setCellValue(tmp.getName());
                    cell01.setCellStyle(cellStyle);
                    HSSFCell cell02 = (HSSFCell)row.createCell(2);
                    cell02.setCellValue(tmp.getLon());
                    cell02.setCellStyle(cellStyle);
                    HSSFCell cell03 = (HSSFCell)row.createCell(3);
                    cell03.setCellValue(tmp.getLat());
                    cell03.setCellStyle(cellStyle);
                    HSSFCell cell04 = (HSSFCell)row.createCell(4);
                    cell04.setCellValue(tmp.getAddress());
                    cell04.setCellStyle(cellStyle);
                    HSSFCell cell05 = (HSSFCell)row.createCell(5);
                    cell05.setCellValue(1); //building type: SDU
                    cell05.setCellStyle(cellStyle);
                    HSSFCell cell06 = (HSSFCell)row.createCell(8);
                    ArrayList<Floor> floors_list = new ArrayList<>();
                    for (Block block : tmp.getBlocks()) {
                        for(Floor floor : block.getFloors()){
                            floors_list.add(floor);
                        }
                    }
                    cell06.setCellValue(floors_list.size());
                    //cell06.setCellValue(buildingList.get(j).getNbFloors());
                    cell06.setCellStyle(cellStyle);
                    HSSFCell cell07 = (HSSFCell)row.createCell(10);
                    //cell07.setCellValue(buildingList.get(j).getNbPrise());
                    cell07.setCellValue(10000);
                    cell07.setCellStyle(cellStyle);
                    HSSFCell cell08 = (HSSFCell)row.createCell(11);
                    //cell08.setCellValue(buildingList.get(j).getNumberInhabitant());
                    cell08.setCellValue(00000);
                    cell08.setCellStyle(cellStyle);

                    HSSFCell cellf6 = (HSSFCell)row.createCell(6);
                    cellf6.setCellStyle(cellStyle);

                    HSSFCell cellf7 = (HSSFCell)row.createCell(7);
                    cellf7.setCellStyle(cellStyle);

//                    if (buildingList.get(j).getResidOrBusiness()==1){
//                        HSSFCell cell09 = (HSSFCell)row.createCell(9);
//                        cell09.setCellValue(buildingList.get(j).getNbPrise());
//                        cell09.setCellStyle(cellStyle);
//                        HSSFCell cellf10 = (HSSFCell)row.createCell(10);
//                        cellf10.setCellStyle(cellStyle);
//                    }else if (buildingList.get(j).getResidOrBusiness()==2){
//                        HSSFCell cellf9 = (HSSFCell)row.createCell(9);
//                        cellf9.setCellStyle(cellStyle);
//                        HSSFCell cell010 = (HSSFCell)row.createCell(10);
//                        cell010.setCellValue(buildingList.get(j).getNbPrise());
//                        cell010.setCellStyle(cellStyle);
//                    }else{
//                        HSSFCell cellf9 = (HSSFCell)row.createCell(9);
//                        cellf9.setCellStyle(cellStyle);
//                        HSSFCell cellf10 = (HSSFCell)row.createCell(10);
//                        cellf10.setCellStyle(cellStyle);
//                    }

                    HSSFCell cell09 = (HSSFCell)row.createCell(9);
                    cell09.setCellValue("nb prise");
                    cell09.setCellStyle(cellStyle);
                    HSSFCell cellf10 = (HSSFCell)row.createCell(10);
                    cellf10.setCellStyle(cellStyle);


                    outlets_cmp = appartments_list.size(); //maj du nb total outlets

                    l=l+1;
                    a=a+1;
                    m=m+1;


                }else{

                    //Integer sizeMerged = buildingList.get(j).getNumberTotalFloors();
                    List<Floor> floors_list = new ArrayList<>();
                    for (Block block : tmp.getBlocks()) {
                        for(Floor floor : block.getFloors()){
                            floors_list.add(floor);
                        }
                    }
                    Integer sizeMerged = floors_list.size();
                    if (sizeMerged != 0){
                        for (int i=0;i<=6;i++){
                            sheet1.addMergedRegion(new CellRangeAddress(
                                    l, //first row (0-based)
                                    l+sizeMerged-1, //last row  (0-based)
                                    i, //first column (0-based)
                                    i  //last column  (0-based)
                            ));
                        }
                        sheet1.addMergedRegion(new CellRangeAddress(
                                l, //first row (0-based)
                                l+sizeMerged-1, //last row  (0-based)
                                11, //first column (0-based)
                                11  //last column  (0-based)
                        ));
                        for (int g=l;g<l+sizeMerged;g++){
                            sheet1.createRow(g);
                        }

                        HSSFRow row = sheet1.getRow(l);
                        HSSFCell cell00 = (HSSFCell)row.createCell(0);
                        cell00.setCellValue(j);
                        cell00.setCellStyle(cellStyle);
                        HSSFCell cell01 = (HSSFCell)row.createCell(1);
                        cell01.setCellValue(tmp.getName());
                        cell01.setCellStyle(cellStyle);
                        HSSFCell cell02 = (HSSFCell)row.createCell(2);
                        cell02.setCellValue(tmp.getLon());
                        cell02.setCellStyle(cellStyle);
                        HSSFCell cell03 = (HSSFCell)row.createCell(3);
                        cell03.setCellValue(tmp.getLat());
                        cell03.setCellStyle(cellStyle);
                        HSSFCell cell04 = (HSSFCell)row.createCell(4);
                        cell04.setCellValue(tmp.getAddress());
                        cell04.setCellStyle(cellStyle);
                        HSSFCell cell05 = (HSSFCell)row.createCell(5);
                        cell05.setCellValue(2);
                        cell05.setCellStyle(cellStyle);
                        HSSFCell cell06 = (HSSFCell)row.createCell(6);
                        cell06.setCellValue(tmp.getBlocks().size()); //blocks size
                        cell06.setCellStyle(cellStyle);
                        HSSFCell cell011 = (HSSFCell)row.createCell(11);
                        cell011.setCellValue(0); //nomber of inhabitants
                        cell011.setCellStyle(cellStyle);
                        HSSFRow rowM;
                        HSSFRow rowA;
                        HSSFCell cell07;
                        HSSFCell cell08;
                        HSSFCell cell09;
                        HSSFCell cell010;

                        int NbResident = 0;
                        int NbBusiness = 0;

                        List<Floor> floors = new ArrayList<>();
                        for (int x=0;x< tmp.getBlocks().size() ;x++){

                            for (Block block : tmp.getBlocks()) {
                                floors.addAll(block.getFloors());
                            }

                            //Integer sizeMerged2 = buildingList.get(j).getNumberTotalFloorsInBloc(x+1);
                            int sizeMerged2 = floors.size();
                            for (int i=7;i<11;i++){
                                if((i!=9)&&(i!=10)){
                                    sheet1.addMergedRegion(new CellRangeAddress(
                                            m, //first row (0-based)
                                            m+sizeMerged2-1, //last row  (0-based)
                                            i, //first column (0-based)
                                            i  //last column  (0-based)
                                    ));
                                }
                            }

                            rowM = sheet1.getRow(m);
                            cell07 = (HSSFCell)rowM.createCell(7);
                            cell07.setCellType(Cell.CELL_TYPE_NUMERIC);
                            cell07.setCellValue(x+1);
                            cell07.setCellStyle(cellStyle);



                            cell08 = (HSSFCell)rowM.createCell(8);
                            cell08.setCellValue(sizeMerged2);
                            cell08.setCellStyle(cellStyle);

                            //	cell011 = (HSSFCell)rowM.createCell(11);
                            //	cell011.setCellValue(buildingList.get(j).getNumberInhabitant().get(x+1));
                            //	cell011.setCellStyle(cellStyle);

                            List<Appartment> business_apprtments_list = new ArrayList<>();
                            List<Appartment> residential_apprtments_list = new ArrayList<>();
                            for(int y=1;y<=sizeMerged2;y++){
                                rowA = sheet1.getRow(a);

                                for (Block block : tmp.getBlocks()) {
                                    for (Floor floor : block.getFloors()){
                                        for (Appartment appartment : floor.getAppartmens()){
                                            if(appartment.getType().equals("BUSINESS")) {
                                                business_apprtments_list.add(appartment);
                                            }else {
                                                residential_apprtments_list.add(appartment);
                                            }
                                        }
                                    }
                                }

                                NbResident = residential_apprtments_list.size();
                                NbBusiness = business_apprtments_list.size();


                                cell09 = (HSSFCell)rowA.createCell(9);
                                cell09.setCellValue(NbResident);
                                cell09.setCellStyle(cellStyle);
                                cell010 = (HSSFCell)rowA.createCell(10);
                                cell010.setCellValue(NbBusiness);
                                cell010.setCellStyle(cellStyle);

                                outlets_cmp = outlets_cmp + NbResident + NbBusiness;

                                a=a+1;
                            }



                            m=m+sizeMerged2;

                        }
                        l=l+sizeMerged;
                    }else{

                        sheet1.createRow(l);
                        HSSFRow row = sheet1.getRow(l);
                        HSSFCell cell00 = (HSSFCell)row.createCell(0);
                        cell00.setCellValue(j);
                        cell00.setCellStyle(cellStyle);
                        HSSFCell cell01 = (HSSFCell)row.createCell(1);
                        cell01.setCellValue(tmp.getName());
                        cell01.setCellStyle(cellStyle);
                        HSSFCell cell02 = (HSSFCell)row.createCell(2);
                        cell02.setCellValue(tmp.getLat());
                        cell02.setCellStyle(cellStyle);
                        HSSFCell cell03 = (HSSFCell)row.createCell(3);
                        cell03.setCellValue(tmp.getLon());
                        cell03.setCellStyle(cellStyle);
                        HSSFCell cell04 = (HSSFCell)row.createCell(4);
                        cell04.setCellValue(tmp.getAddress());
                        cell04.setCellStyle(cellStyle);
                        HSSFCell cell05 = (HSSFCell)row.createCell(5);
                        cell05.setCellValue(2);
                        cell05.setCellStyle(cellStyle);
                        HSSFCell cell06 = (HSSFCell)row.createCell(6);
                        cell06.setCellValue(0);
                        cell06.setCellStyle(cellStyle);
                        HSSFCell cell07 = (HSSFCell)row.createCell(7);
                        cell07.setCellValue(0);
                        cell07.setCellStyle(cellStyle);
                        HSSFCell cell08 = (HSSFCell)row.createCell(8);
                        cell08.setCellValue(0);
                        cell08.setCellStyle(cellStyle);
                        HSSFCell cell09 = (HSSFCell)row.createCell(9);
                        cell09.setCellValue(0);
                        cell09.setCellStyle(cellStyle);
                        HSSFCell cell010 = (HSSFCell)row.createCell(10);
                        cell010.setCellValue(0);
                        cell010.setCellStyle(cellStyle);
                        HSSFCell cell011 = (HSSFCell)row.createCell(11);
                        cell011.setCellValue(0);
                        cell011.setCellStyle(cellStyle);
                    }

                }


            }


        }else{

        }

        cellR2_2.setCellValue(outlets_cmp);// maj du champs indiquant le nb total d outlets



        return wb;
    }



    public  void writeSheet() {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            // Do the file write
            try {
                String uuid = UUID.randomUUID().toString().substring(6);

                String outFileName = "file"+uuid+".xls";

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/mark_files");
                myDir.mkdirs();


               //File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

                File ff = new File(myDir, outFileName);
                if (ff.exists()){
                    ff.delete();
                }
                ff.createNewFile();
                FileOutputStream outFile = new FileOutputStream(ff);
                render().write(outFile);
                outFile.flush();
                outFile.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            // Request permission from the user
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);

        }


    }

}
