package com.qosdesign.markbuilding.utils;
import com.google.android.gms.maps.model.Marker;
import com.qosdesign.markbuilding.data.pojo.Building;


import java.util.HashMap;
import java.util.Iterator;

public class Markers extends HashMap<Marker,Object> {

    public Markers getBuildings(){
        Markers tmp = new Markers() ;
        for(Marker marker : this.keySet()){
            if(this.get(marker) instanceof Building){
                tmp.put(marker,this.get(marker)) ;

            }
        }
        return tmp ;
    }

    public void deleteAllBuildings(){

        for(Marker marker : this.keySet()){
            if(this.get(marker) instanceof Building){
                marker.remove();
            }
        }
    }

    public void deleteSelectedBuildings(Building building){

        Iterator<Marker> it = this.keySet().iterator();
        while (it.hasNext()){
            Marker marker = it.next();
            if(marker.getTitle().equals(building.getName())){
                marker.remove();
                it.remove();
            }
        }

    }



}
