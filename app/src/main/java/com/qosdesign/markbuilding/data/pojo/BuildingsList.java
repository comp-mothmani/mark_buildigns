package com.qosdesign.markbuilding.data.pojo;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "buildings"
})
public class BuildingsList implements Serializable {

    @JsonProperty("buildings")
    private List<Building> buildings = new ArrayList<Building>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -5456791565835172392L;

    @JsonProperty("buildings")
    public List<Building> getBuildings() {
        return buildings;
    }
    @JsonProperty("buildings")
    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void removeBuilding(Building val){
        for (Building tmp: buildings) {
            if (tmp.getName() == val.getName()) {
                val = tmp;
            }
        }
        this.buildings.remove(val) ;
    }


}
