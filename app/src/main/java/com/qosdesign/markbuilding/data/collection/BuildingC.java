package com.qosdesign.markbuilding.data.collection;

import com.qosdesign.markbuilding.data.pojo.Building;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class BuildingC extends ArrayList<Building> {

    public BuildingC(Collection<Building> vals){
        this.addAll(vals) ;
    }

    public BuildingC() {

    }

    public Building getFirst(){
        return this.get(0) ;
    }


    public BuildingC filterByName(String val){
        BuildingC tmps = new BuildingC() ;
        for(Building tmp : this){
            if(tmp.getName().equals(val)){
                tmps.add(tmp) ;
            }
        }
        return tmps ;
    }

    public BuildingC sortTasks() {
        Collections.sort(this, new Comparator<Building>() {
            public int compare(Building p1, Building p2) {
                return String.valueOf(p1.getName()).compareTo(p2.getName());
            }
        });
        return this;
    }
}
