package com.qosdesign.markbuilding.data.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "rank",
        "appartmens",
        "type"
})
public class Floor implements Serializable
{

    @JsonProperty("name")
    private String name;
    @JsonProperty("rank")
    private Integer rank;
    @JsonProperty("appartmens")
    private List<Appartment> appartmens = new ArrayList<Appartment>();
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 1554801599891412399L;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("rank")
    public Integer getRank() {
        return rank;
    }

    @JsonProperty("rank")
    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @JsonProperty("appartmens")
    public List<Appartment> getAppartmens() {
        return appartmens;
    }

    @JsonProperty("appartmens")
    public void setAppartmens(List<Appartment> appartmens) {
        this.appartmens = appartmens;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void removeAppartment(Appartment val){
        for (Appartment tmp: appartmens) {
            if (tmp.getRank() == val.getRank()) {
                val = tmp;
            }
        }
        this.appartmens.remove(val) ;
    }

}