package com.qosdesign.markbuilding.data.collection;

import com.qosdesign.markbuilding.data.pojo.Appartment;

import java.util.ArrayList;
import java.util.Collection;

public class AppartmentC extends ArrayList<Appartment> {

    public AppartmentC(Collection<Appartment> vals){
        this.addAll(vals) ;
    }

    public AppartmentC() {

    }

    public Appartment getFirst(){
        return this.get(0) ;
    }

    public AppartmentC filterByRank(String val){
        AppartmentC tmps = new AppartmentC() ;
        for(Appartment tmp : this){
            if(tmp.getRank().equals(val)){
                tmps.add(tmp) ;
            }
        }
        return tmps ;
    }

    public AppartmentC filterByType(String val){
        AppartmentC tmps = new AppartmentC() ;
        for(Appartment tmp : this){
            if(tmp.getType().equals(val)){
                tmps.add(tmp) ;
            }
        }
        return tmps ;
    }
}
