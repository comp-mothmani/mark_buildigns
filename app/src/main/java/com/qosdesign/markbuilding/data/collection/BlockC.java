package com.qosdesign.markbuilding.data.collection;

import com.qosdesign.markbuilding.data.pojo.Block;

import java.util.ArrayList;
import java.util.Collection;

public class BlockC extends ArrayList<Block> {

    public BlockC(Collection<Block> vals){
        this.addAll(vals) ;
    }

    public BlockC() {

    }

    public Block getFirst(){
        return this.get(0) ;
    }

    public BlockC filterByName(String val){
        BlockC tmps = new BlockC() ;
        for(Block tmp : this){
            if(tmp.getName().equals(val)){
                tmps.add(tmp) ;
            }
        }
        return tmps ;
    }
}
