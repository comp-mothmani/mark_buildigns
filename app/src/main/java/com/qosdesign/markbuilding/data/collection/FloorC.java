package com.qosdesign.markbuilding.data.collection;

import com.qosdesign.markbuilding.data.pojo.Floor;

import java.util.ArrayList;
import java.util.Collection;

public class FloorC extends ArrayList<Floor> {
    
    public FloorC(Collection<Floor> vals){
        this.addAll(vals) ;
    }

    public FloorC() {

    }

    public Floor getFirst(){
        return this.get(0) ;
    }


    public FloorC filterByName(String val){
        FloorC tmps = new FloorC() ;
        for(Floor tmp : this){
            if(tmp.getName().equals(val)){
                tmps.add(tmp) ;
            }
        }
        return tmps ;
    }
}
