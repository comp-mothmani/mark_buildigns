package com.qosdesign.markbuilding.data.pojo;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.android.gms.maps.model.LatLng;
import com.qosdesign.markbuilding.data.collection.AppartmentC;
import com.qosdesign.markbuilding.data.collection.BlockC;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "note",
        "lat",
        "lon",
        "address",
        "type",
        "blocks",
        "residenceOrBusiness"
})
public class Building implements Serializable
{

    @JsonProperty("name")
    private String name;
    @JsonProperty("note")
    private String note;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lon")
    private String lon;
    @JsonProperty("address")
    private String address;
    @JsonProperty("type")
    private String type;
    @JsonProperty("residenceOrBusiness")
    private int residenceOrBusiness;
    @JsonProperty("blocks")
    private List<Block> blocks = new ArrayList<Block>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -5456791565835172392L;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("note")
    public String getNote() {
        return note;
    }

    @JsonProperty("note")
    public void setNote(String note) {
        this.note = note;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lon")
    public String getLon() {
        return lon;
    }

    @JsonProperty("lon")
    public void setLon(String lon) {
        this.lon = lon;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("residenceOrBusiness")
    public int getResidenceOrBusiness() {
        return residenceOrBusiness;
    }
    @JsonProperty("residenceOrBusiness")
    public void setResidenceOrBusiness(int residenceOrBusiness) {
        this.residenceOrBusiness = residenceOrBusiness;
    }

    @JsonProperty("blocks")
    public List<Block> getBlocks() {
        return blocks;
    }

    @JsonProperty("blocks")
    public void setBlocks(List<Block> blocks) {
        this.blocks = blocks;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public double lat(){
        return Double.parseDouble(lat) ;
    }

    public double lon(){
        return Double.parseDouble(lon) ;
    }

    public LatLng position() {
        return new LatLng(lat(), lon());
    }

    public void removeBlock(Block val){
        for (Block tmp: blocks) {
            if (tmp.getName() == val.getName()) {
                val = tmp;
            }
        }
        this.blocks.remove(val) ;
    }


    public Integer getNumberTotalFloorsInBloc(int i) {
        if (getBlocks().size() < i){
            return 0 ;
        }
        Block bk = getBlocks().get(i - 1) ;
        return bk.getFloors().size() ;
    }

    public int getNBResidentPerFloorPerBloc(int i, int y) {
       return new AppartmentC(new BlockC(getBlocks()).get(i-1).getFloors().get(y - 1).getAppartmens()).filterByType("RESIDENTIAL").size();
    }

    public int getNBBusinessPerFloorPerBloc(int i, int y) {
        return new AppartmentC(new BlockC(getBlocks()).get(i-1).getFloors().get(y - 1).getAppartmens()).filterByType("BUSINESS").size();
    }

    public String getNumberInhabitant() {
        return "0" ;
    }
}