package com.qosdesign.markbuilding;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.qosdesign.markbuilding.ccmp.AbstractMapActivity;
import com.qosdesign.markbuilding.ccmp.CMenuItem;
import com.qosdesign.markbuilding.ccmp.CMenuItemClickListener;
import com.qosdesign.markbuilding.ccmp.CMenuItemTypes;
import com.qosdesign.markbuilding.ccmp.dialog.CDialog;
import com.qosdesign.markbuilding.ccmp.dialog.DialogBack;
import com.qosdesign.markbuilding.data.collection.BuildingC;
import com.qosdesign.markbuilding.data.pojo.Appartment;
import com.qosdesign.markbuilding.data.pojo.Block;
import com.qosdesign.markbuilding.data.pojo.Building;
import com.qosdesign.markbuilding.data.pojo.BuildingsList;
import com.qosdesign.markbuilding.data.pojo.Floor;
import com.qosdesign.markbuilding.utils.AdvancedTreeNode.TreeNode;
import com.qosdesign.markbuilding.utils.AdvancedTreeNode.TreeView;
import com.qosdesign.markbuilding.utils.AdvancedTreeNode.base.BaseNodeViewBinder;
import com.qosdesign.markbuilding.utils.AdvancedTreeNode.base.BaseNodeViewFactory;
import com.qosdesign.markbuilding.utils.AdvancedTreeNode.base.CheckableNodeViewBinder;
import com.qosdesign.markbuilding.utils.PermissionUtils;
import com.qosdesign.markbuilding.utils.generator.GenerateXlsx;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class MBMapsActivity extends AbstractMapActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener, GoogleMap.OnCameraChangeListener, Switch.OnCheckedChangeListener {

    protected CustomAdapter customAdapter;
    protected ImageButton btn_add;
    protected ListView listView;
    protected Button generte_btn;
    private DataModel selectedItem;
    private TreeView block_treeView;
    private ViewGroup blocksViewGroup;
    private ImageButton btn_remove;
    private Building building_to_edit;
    private Switch switch_;
    private Boolean switchState;

    @Override
    public void initData() {

        CMenuItem sb2 = new CMenuItem(CMenuItemTypes.HeaderMenuItem, 1, "GPS", new CMenuItemClickListener() {
            @Override
            public void clickEvent(CMenuItem it) {
                Toast.makeText(getApplicationContext(), "GPS is Activated", Toast.LENGTH_LONG).show();
                initLocationActionBarItem();
            }
        });
        sb2.setGrpId(0);
        sb2.setIcon(R.drawable.gpsnewred);
        sb2.setOrder(0);
        sb2.attach(this);
        items.add(sb2);

        sb2 = new CMenuItem(CMenuItemTypes.HeaderMenuItem, 2, "MAP_TYPE", new CMenuItemClickListener() {
            @Override
            public void clickEvent(CMenuItem it) {
                Toast.makeText(getApplicationContext(), "Choose Map type", Toast.LENGTH_LONG).show();
                showStylesDialog();
            }
        });
        sb2.setGrpId(0);
        sb2.setIcon(R.drawable.worldwide);
        sb2.setOrder(0);
        sb2.attach(this);
        items.add(sb2);


//        for (int i = 0; i < 300; i++) {
//
//            DataModel model = new DataModel();
//            model.setSelected(false);
//            model.setBuilding_name("Building " + i);
//            modelArrayList.add(model);
//        }




    }

    @Override
    public String actionBarTitle() {
        return "Mark Buildings";
    }

    @Override
    public void initViews() {
        map_type_txt = findViewById(R.id.map_type_txt);
        map_type_txt.bringToFront();

        dl = findViewById(R.id.activity_main);
        dl.setScrimColor(Color.TRANSPARENT);  //fade color transparant.

        nv = findViewById(R.id.nv);

        // INIT GENERATE XLSX BUTTON
        generte_btn = nv.findViewById(R.id.generte_btn);
        generte_btn.setOnClickListener(this);

        // INIT ADD BUILDING BUTTON
        btn_add = nv.findViewById(R.id.add_building_btn);
        btn_add.setOnClickListener(this);

        // INIT ADD BUILDING BUTTON
        btn_remove = nv.findViewById(R.id.delete_building_btn);
        btn_remove.setOnClickListener(this);

        // INIT BUILDING LISTVIEW
        listView = nv.findViewById(R.id.list);
        customAdapter = new CustomAdapter(modelArrayList, getApplicationContext());
        listView.setAdapter(customAdapter);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        //INIT SWITCH VIEW
        switch_ = nv.findViewById(R.id.switch1);
        switch_.setOnCheckedChangeListener(this);
        switchState = true;



    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.add_building_btn) {
            Toast.makeText(getApplicationContext(), "Add New Building", Toast.LENGTH_LONG).show();
            addNewBuidingAlertDialog();

//            MBMapsActivity.DialogAddBuilding dial = new MBMapsActivity.DialogAddBuilding();
//            dial.newInstance(0, new DialogBack() {
//                @Override
//                public void back(CDialog dialogF) {
//
//                }
//
//                @Override
//                public void save(CDialog dialogF) {
//
//                    MBMapsActivity.DialogAddBuilding dial2 = new MBMapsActivity.DialogAddBuilding();
//                    dial2.newInstance(0, new DialogBack() {
//                        @Override
//                        public void back(CDialog dialogF) {
//
//                        }
//
//                        @Override
//                        public void save(CDialog dialogF) {
//
//                        }
//                    });
//                    dial2.show(getFragmentManager(), "");
//                }
//            });
//            dial.show(getFragmentManager(), "");
        } else if (view.getId() == R.id.generte_btn) {

            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                if(!checkedItems.isEmpty()) {

                    generateXlsAlert();

                }else {
                    Toast.makeText(getApplicationContext(), "No items selected.", Toast.LENGTH_LONG).show();
                }
            }
            else {
                // Request permission from the user
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }



            /*Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"email@example.com"});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "subject here");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "body text");
            File root = Environment.getExternalStorageDirectory();
            String pathToMyAttachedFile = "temp/attachement.xml";
            File file = new File(root, pathToMyAttachedFile);
            if (!file.exists() || !file.canRead()) {
                return;
            }
            Uri uri = Uri.fromFile(file);
            emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(emailIntent, "Pick an Email provider"));*/

        } else if (view.getId() == R.id.delete_building_btn) {

            AlertDialog.Builder builder = new AlertDialog.Builder(MBMapsActivity.this) ;
            builder.setMessage("Are you sure you want to remove the selected Building/s ?") ;
            builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    for (DataModel ss : checkedItems) {
                        customAdapter.remove(ss);
                        Building selected_building = new BuildingC(buildings_list.getBuildings()).filterByName(ss.getBuilding_name()).getFirst();
                        markers.deleteSelectedBuildings(selected_building);
                        buildings_list.removeBuilding(selected_building);

                    }

                    customAdapter.notifyDataSetChanged();
                    createSharedPref(buildings_list);
                    checkedItems.clear();

                    dialog.cancel();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();

                }
            });

            AlertDialog dialog = builder.create() ;
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();


        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(getApplicationContext(), "position:   " + i, Toast.LENGTH_LONG).show();

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

        selectedItem = modelArrayList.get(i);
        Toast.makeText(getApplicationContext(), selectedItem.getBuilding_name(), Toast.LENGTH_LONG).show();
        // building_to_edit = new BuildingC(buildings_list.getBuildings()).filterByName(selectedItem.building_name).getFirst();
         Building building = new BuildingC(buildings_list.getBuildings()).filterByName(selectedItem.building_name).getFirst();
         AppContext.CBUILDING = building;
         editBuidingAlertDialog(building);
        return true;
    }

    @Override
    public void onMapClick(final LatLng latLng) {
//        int heightO = 50;
//        int widthO = 35;
//        BitmapDrawable bitmapdrawcs = (BitmapDrawable) getResources().getDrawable(R.drawable.buildings);
//        Bitmap bcs = bitmapdrawcs.getBitmap();
//        Bitmap csicon = Bitmap.createScaledBitmap(bcs, widthO, heightO, false);
//        String name = "B"+ UUID.randomUUID().toString().substring(2);
//        Marker marker = mMap.addMarker(new MarkerOptions()
//                .position(new LatLng(latLng.latitude, latLng.longitude))
//                .title(name).icon(BitmapDescriptorFactory.fromBitmap(csicon)));
//        Building building = new Building();
//        building.setName(name);
//        markers.put(marker, building);
//
//        MBMapsActivity.DataModel model = new MBMapsActivity.DataModel();
//        model.setSelected(false);
//        model.setBuilding_name(name);
//
//        customAdapter.notifyDataSetChanged();

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            if(b){
                titlMap();
                customAdapter.clear();
                customAdapter.notifyDataSetChanged();
                mMap.setOnCameraChangeListener(null);
                Toast.makeText(getApplicationContext(), "SHOW ALL BUILDINGS", Toast.LENGTH_LONG).show();

                for (Building tmp : buildings_list.getBuildings()){
                    MBMapsActivity.DataModel model = new MBMapsActivity.DataModel();
                    model.setSelected(false);
                    model.setBuilding_name(tmp.getName());
                    customAdapter.add(model);
                    customAdapter.notifyDataSetChanged();
                }


            }else {
                customAdapter.clear();
                customAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "SHOW ONLY BUILDINGS IN MAP BOUNDS", Toast.LENGTH_LONG).show();
                mMap.setOnCameraChangeListener(this);
                titlMap();

            }
    }

    public static class DataModel {

        String building_name;

        private boolean isSelected;

        public DataModel(String building_name) {
            this.building_name = building_name;
        }

        public DataModel() {

        }

        public String getBuilding_name() {
            return building_name;
        }

        public void setBuilding_name(String building_name) {
            this.building_name = building_name;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }

    public class CustomAdapter extends ArrayAdapter<DataModel> {

        private Context context;
        public ArrayList<DataModel> modelArrayList;

        public CustomAdapter(ArrayList<DataModel> modelArrayList, Context context) {
            super(context, R.layout.building_item, modelArrayList);
            this.modelArrayList = modelArrayList;
            this.context = context;
        }

        @Override
        public int getViewTypeCount() {
            // return getCount();
            if(getCount() > 0){
                return getCount();
            }else{
                return super.getViewTypeCount();
            }
        }

        @Override
        public int getItemViewType(int position) {

            return position;
        }

        @Override
        public int getCount() {
            return modelArrayList.size();
        }

        @Override
        public DataModel getItem(int position) {
            return modelArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.building_item, null, true);

                holder.cbx = convertView.findViewById(R.id.b_checkbx);
                holder.txt = convertView.findViewById(R.id.b_name);

                convertView.setTag(holder);
            } else {
                // the getTag returns the viewHolder object set as a tag to the view
                holder = (ViewHolder) convertView.getTag();
            }

            holder.txt.setText(modelArrayList.get(position).getBuilding_name());

            holder.cbx.setChecked(modelArrayList.get(position).isSelected());

            holder.cbx.setTag(R.integer.btnplusview, convertView);
            holder.cbx.setTag(position);
            holder.cbx.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    View tempview = (View) holder.cbx.getTag(R.integer.btnplusview);
                    TextView tv = tempview.findViewById(R.id.b_name);
                    Integer pos = (Integer) holder.cbx.getTag();
                    Toast.makeText(context, "Checkbox " + pos + " clicked!", Toast.LENGTH_SHORT).show();

                    DataModel ss = modelArrayList.get(pos);
                    //checkedItems.add(ss);

                    Building selected_building = new BuildingC(buildings_list.getBuildings()).filterByName(ss.getBuilding_name()).getFirst();

                    if (modelArrayList.get(pos).isSelected) {
                        modelArrayList.get(pos).setSelected(false);
                        checkedItems.remove(ss);
                        AppContext.BUILDINGS_LIST.getBuildings().remove(selected_building);
                    } else {
                        modelArrayList.get(pos).setSelected(true);
                        checkedItems.add(ss);
                        AppContext.BUILDINGS_LIST.getBuildings().add(selected_building);
                    }
                }
            });

            return convertView;
        }

        private class ViewHolder {

            protected CheckBox cbx;
            private TextView txt;

        }
    }

    @SuppressLint("ValidFragment")
    public class DialogAddBuilding extends CDialog {

        private DialogBack back;
        private Button btn_position;
        private TextView mark_lat;
        private TextView mark_lon;
        private TextView mark_address;
        private EditText building_name;
        private EditText building_note;
        private Spinner building_type_spin;
        private BottomNavigationView btm;
        private Button add_block_btn;

        public DialogAddBuilding newInstance(int title, DialogBack back) {
            this.back = back;

            DialogAddBuilding instance = new DialogAddBuilding();
            setStyle(DialogFragment.STYLE_NO_TITLE, 0);
            Bundle args = new Bundle();
            args.putInt("title", title);
            instance.setArguments(args);
            return instance;
        }

        @Override
        public int setLayoutView() {
            return R.layout.add_new_building_layout;
        }

        @Override
        public void initViews() {

            btn_position = view.findViewById(R.id.button_position);
            mark_lat = view.findViewById(R.id.mark_lat);
            mark_lon = view.findViewById(R.id.mark_lon);
            mark_address = view.findViewById(R.id.mark_address);
            building_name = view.findViewById(R.id.buildingname);
            building_note = view.findViewById(R.id.description);
            building_type_spin = view.findViewById(R.id.building_type_spin);
            blocksViewGroup = view.findViewById(R.id.block_container);
           // btm = view.findViewById(R.id.btmNav);
            add_block_btn = view.findViewById(R.id.add_block_btn);

        }

        @Override
        public void fill() {
            ///use this method to pass data to window
            //selectedItem
            final Building building = new Building();
            AppContext.CBUILDING = building;
            btn_position.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Enable the location layer. Request the location permission if needed.
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);

                        Location loc = mMap.getMyLocation();
                        if (loc != null) {
                            LatLng latLng = new LatLng(loc.getLatitude(), loc
                                    .getLongitude());
                            mark_lat.setText("" + latLng.latitude);
                            mark_lon.setText("" + latLng.longitude);
                            mark_address.setText(getCompleteAddressString(latLng.latitude, latLng.longitude));
                        } else {
                            mark_lat.setText("N/A");
                            mark_lon.setText("N/A");
                            mark_address.setText("N/A");
                            Toast.makeText(getApplicationContext(), "***SYSTEM COULD NOT RETRIEVE LOCATION DATA*** " +
                                    "\nPossible Causes:" +
                                    "\n**GPS is not enabled on your device ==> ENABLE GPS" +
                                    "\n**GPS functionnality does not work properly INDOORS ==> Try again OUTDOORS", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        // Uncheck the box until the layer has been enabled and request missing permission.
                        PermissionUtils.requestPermission(MBMapsActivity.this, LOCATION_PERMISSION_REQUEST_CODE,
                                Manifest.permission.ACCESS_FINE_LOCATION, false);
                    }
                }
            });

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_white_text, building_type_array);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            building_type_spin.setAdapter(dataAdapter);

            add_block_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    addNewBlockAlert(blocksViewGroup, building);


                }
            });

            btm.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    if (item.getItemId() == R.id.back) {
                        dismiss();

                    } else if (item.getItemId() == R.id.save) {

                        if(mark_address.getText().toString().equals("N/A")) {
                            Toast.makeText(getApplicationContext(), "Building ADDRESS is missing", Toast.LENGTH_LONG).show();
                        }else if(mark_lat.getText().toString().equals("N/A")) {
                            Toast.makeText(getApplicationContext(), "Building LATITUDE is missing", Toast.LENGTH_LONG).show();
                        }else if(mark_lon.getText().toString().equals("N/A")) {
                            Toast.makeText(getApplicationContext(), "Building LONGITUDE is missing", Toast.LENGTH_LONG).show();
                        }else if(building_name.getText().toString().equals("")) {
                            Toast.makeText(getApplicationContext(), "Building NAME is missing", Toast.LENGTH_LONG).show();
                        }else if(building_note.getText().toString().equals("")) {
                            Toast.makeText(getApplicationContext(), "Building NOTE is missing", Toast.LENGTH_LONG).show();
                        }else if(building.getBlocks().isEmpty()){
                            Toast.makeText(getApplicationContext(), "Every BUILDING, must have at least one BLOCK. ", Toast.LENGTH_LONG).show();
                        }else if(AppContext.CBLOCK.getFloors().isEmpty()){
                            Toast.makeText(getApplicationContext(), "Every BLOCK, must have at least one FLOOR. ", Toast.LENGTH_LONG).show();
                        } else {
                            building.setAddress(mark_address.getText().toString());
                            building.setLat(mark_lat.getText().toString());
                            building.setLon(mark_lon.getText().toString());
                            building.setName(building_name.getText().toString());
                            building.setNote(building_note.getText().toString());
                            String building_type = building_type_spin.getSelectedItem().toString();
                            building.setType(building_type);
                            buildings_list.getBuildings().add(building);
                            createSharedPref(buildings_list);
                            dismiss();

                            MBMapsActivity.DataModel model = new MBMapsActivity.DataModel();
                            model.setSelected(false);
                            model.setBuilding_name(building.getName());
                            customAdapter.add(model);


                            drawBuilding(building);
                            createCircle(new LatLng(building.lat(), building.lon()), (double) 10);

                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(building.lat(), building.lon()), 15));

                        }

                    }

                    return true;
                }
            });


        }

        @Override
        public void reverseFill() {
            /// use to create data from window
        }

    }

    public void addNewBuidingAlertDialog(){

        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = inflater.inflate(R.layout.add_new_building_layout, null);
        builder.setView(view);

        /***
         * custom title for alert dialogue
         */
        final View customTitleView = inflater.inflate(R.layout.custom_title, null);
        TextView title = customTitleView.findViewById(R.id.customtitle);
        ImageView img = customTitleView.findViewById(R.id.imageView3);
        title.setText("Add New Building");
        img.setImageResource(R.drawable.hotel_white);
        builder.setCustomTitle(customTitleView);

        Button btn_position = view.findViewById(R.id.button_position);
        final TextView mark_lat = view.findViewById(R.id.mark_lat);
        final TextView mark_lon = view.findViewById(R.id.mark_lon);
        final EditText building_name = view.findViewById(R.id.buildingname);
        final EditText building_note = view.findViewById(R.id.description);
        final EditText building_adress = view.findViewById(R.id.building_address_edit);
        final Spinner building_type_spin = view.findViewById(R.id.building_type_spin);
        blocksViewGroup = view.findViewById(R.id.block_container);
        Button add_block_btn = view.findViewById(R.id.add_block_btn);

        ///init data
        final Building building = new Building();
        AppContext.CBUILDING = building;
        btn_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Enable the location layer. Request the location permission if needed.
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);

                    Location loc = mMap.getMyLocation();
                    if (loc != null) {
                        LatLng latLng = new LatLng(loc.getLatitude(), loc
                                .getLongitude());
                        mark_lat.setText("" + latLng.latitude);
                        mark_lon.setText("" + latLng.longitude);
                        building_adress.setText(getCompleteAddressString(latLng.latitude, latLng.longitude));
                    } else {
                        mark_lat.setText("N/A");
                        mark_lon.setText("N/A");
                        building_adress.setText("N/A");
                        Toast.makeText(getApplicationContext(), "***SYSTEM COULD NOT RETRIEVE LOCATION DATA*** " +
                                "\nPossible Causes:" +
                                "\n**GPS is not enabled on your device ==> ENABLE GPS" +
                                "\n**GPS functionnality does not work properly INDOORS ==> Try again OUTDOORS", Toast.LENGTH_LONG).show();
                    }

                } else {
                    // Uncheck the box until the layer has been enabled and request missing permission.
                    PermissionUtils.requestPermission(MBMapsActivity.this, LOCATION_PERMISSION_REQUEST_CODE,
                            Manifest.permission.ACCESS_FINE_LOCATION, false);
                }
            }
        });

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_white_text, building_type_array);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        building_type_spin.setAdapter(dataAdapter);

        add_block_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewBlockAlert(blocksViewGroup, building);
            }
        });


        builder.setPositiveButton("Save", null);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(building_adress.getText().toString().equals("N/A")) {
                        Toast.makeText(getApplicationContext(), "Building ADDRESS is missing", Toast.LENGTH_LONG).show();
                    }else if(mark_lat.getText().toString().equals("N/A")) {
                        Toast.makeText(getApplicationContext(), "Building LATITUDE is missing", Toast.LENGTH_LONG).show();
                    }else if(mark_lon.getText().toString().equals("N/A")) {
                        Toast.makeText(getApplicationContext(), "Building LONGITUDE is missing", Toast.LENGTH_LONG).show();
                    }else if(building_name.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "Building NAME is missing", Toast.LENGTH_LONG).show();
                    }else if(building_note.getText().toString().equals("")) {
                        Toast.makeText(getApplicationContext(), "Building NOTE is missing", Toast.LENGTH_LONG).show();
                    }else if(building.getBlocks().isEmpty()){
                        Toast.makeText(getApplicationContext(), "Every BUILDING, must have at least one BLOCK. ", Toast.LENGTH_LONG).show();
                    }else if(AppContext.CBLOCK.getFloors().isEmpty()){
                        Toast.makeText(getApplicationContext(), "Every BLOCK, must have at least one FLOOR. ", Toast.LENGTH_LONG).show();
                    } else {
                        building.setAddress(building_adress.getText().toString());
                        building.setLat(mark_lat.getText().toString());
                        building.setLon(mark_lon.getText().toString());
                        building.setName(building_name.getText().toString());
                        building.setNote(building_note.getText().toString());
                        String building_type = building_type_spin.getSelectedItem().toString();
                        building.setType(building_type);
                        buildings_list.getBuildings().add(building);
                        createSharedPref(buildings_list);

                        MBMapsActivity.DataModel model = new MBMapsActivity.DataModel();
                        model.setSelected(false);
                        model.setBuilding_name(building.getName());
                        customAdapter.add(model);


                        drawBuilding(building);
                        createCircle(new LatLng(building.lat(), building.lon()), (double) 10);

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(building.lat(), building.lon()), 15));

                        dialog.cancel();
                    }
            }
        });
    }

    @SuppressLint("ValidFragment")
    public class DialogEditBuilding extends CDialog {

        private DialogBack back;
        private Button btn_position;
        private TextView mark_lat;
        private TextView mark_lon;
        private TextView mark_address;
        private TextView building_name_txt;
        private EditText building_name;
        private EditText building_note;
        private Spinner building_type_spin;
        private BottomNavigationView btm;
        private Button add_block_btn;

        public DialogEditBuilding newInstance(int title, DialogBack back) {
            this.back = back;

            DialogEditBuilding instance = new DialogEditBuilding();
            setStyle(DialogFragment.STYLE_NO_TITLE, 0);
            Bundle args = new Bundle();
            args.putInt("title", title);
            instance.setArguments(args);
            return instance;
        }

        @Override
        public int setLayoutView() {
            return R.layout.edit_bulding_layout;
        }

        @Override
        public void initViews() {
            btn_position = view.findViewById(R.id.button_position);
            mark_lat = view.findViewById(R.id.mark_lat);
            mark_lon = view.findViewById(R.id.mark_lon);
            mark_address = view.findViewById(R.id.mark_address);
            building_name = view.findViewById(R.id.buildingname);
            building_note = view.findViewById(R.id.description);
            building_type_spin = view.findViewById(R.id.building_type_spin);
            blocksViewGroup = view.findViewById(R.id.block_container);
            btm = view.findViewById(R.id.btmNav);
            add_block_btn = view.findViewById(R.id.add_block_btn);
            building_name_txt = view.findViewById(R.id.building_name_txt);
        }

        @Override
        public void fill() {
            ///use this method to pass data to window
            //selectedItem
        }

        @Override
        public void reverseFill() {
            /// use to create data from window
            btn_position.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Enable the location layer. Request the location permission if needed.
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);

                        Location loc = mMap.getMyLocation();
                        if (loc != null) {
                            LatLng latLng = new LatLng(loc.getLatitude(), loc
                                    .getLongitude());
                            mark_lat.setText("" + latLng.latitude);
                            mark_lon.setText("" + latLng.longitude);
                            mark_address.setText(getCompleteAddressString(latLng.latitude, latLng.longitude));
                        } else {
                            mark_lat.setText("N/A");
                            mark_lon.setText("N/A");
                            mark_address.setText("N/A");
                            Toast.makeText(getApplicationContext(), "***SYSTEM COULD NOT RETRIEVE LOCATION DATA*** " +
                                    "\nPossible Causes:" +
                                    "\n**GPS is not enabled on your device ==> ENABLE GPS" +
                                    "\n**GPS functionnality does not work properly INDOORS ==> Try again OUTDOORS", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        // Uncheck the box until the layer has been enabled and request missing permission.
                        PermissionUtils.requestPermission(MBMapsActivity.this, LOCATION_PERMISSION_REQUEST_CODE,
                                Manifest.permission.ACCESS_FINE_LOCATION, false);
                    }
                }
            });

            add_block_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    addNewBlockAlert(blocksViewGroup, building_to_edit);

                }
            });

            building_name_txt.setText(building_to_edit.getName());
            mark_lat.setText(building_to_edit.getLat());
            mark_lon.setText(building_to_edit.getLon());
            mark_address.setText(building_to_edit.getAddress());
            building_name.setText(building_to_edit.getName());
            building_note.setText(building_to_edit.getNote());

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_white_text, building_type_array);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            building_type_spin.setAdapter(dataAdapter);

            if(building_to_edit.getType().equals("MDU")){
                building_type_spin.setSelection(building_type_array.indexOf("MDU"));
            }else  {
                building_type_spin.setSelection(building_type_array.indexOf("SDU"));
            }
            initBlocksTree(blocksViewGroup, building_to_edit);

            btm.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    if (item.getItemId() == R.id.back) {
                        dismiss();

                    } else if (item.getItemId() == R.id.save) {

                        if(mark_address.getText().toString().equals("N/A")) {
                            Toast.makeText(getApplicationContext(), "Building ADDRESS is missing", Toast.LENGTH_LONG).show();
                        }else if(mark_lat.getText().toString().equals("N/A")) {
                            Toast.makeText(getApplicationContext(), "Building LATITUDE is missing", Toast.LENGTH_LONG).show();
                        }else if(mark_lon.getText().toString().equals("N/A")) {
                            Toast.makeText(getApplicationContext(), "Building LONGITUDE is missing", Toast.LENGTH_LONG).show();
                        }else if(building_name.getText().toString().equals("")) {
                            Toast.makeText(getApplicationContext(), "Building NAME is missing", Toast.LENGTH_LONG).show();
                        }else if(building_note.getText().toString().equals("")) {
                            Toast.makeText(getApplicationContext(), "Building NOTE is missing", Toast.LENGTH_LONG).show();
                        }
                        else if(building_to_edit.getBlocks().isEmpty()){
                            Toast.makeText(getApplicationContext(), "Every BUILDING, must have at least one BLOCK. ", Toast.LENGTH_LONG).show();
                        }else if(AppContext.CBLOCK.getFloors().isEmpty()){
                            Toast.makeText(getApplicationContext(), "Every BLOCK, must have at least one FLOOR. ", Toast.LENGTH_LONG).show();
                        } else {
                            building_to_edit.setAddress(mark_address.getText().toString());
                            building_to_edit.setLat(mark_lat.getText().toString());
                            building_to_edit.setLon(mark_lon.getText().toString());
                            building_to_edit.setName(building_name.getText().toString());
                            building_to_edit.setNote(building_note.getText().toString());
                            String building_type = building_type_spin.getSelectedItem().toString();
                            try {
                                building_to_edit.setType(building_type);
                            }catch (Exception e){
                                building_to_edit.setType("MDU");
                            }

                            createSharedPref(buildings_list);
                            dismiss();

                            selectedItem.setSelected(false);
                            selectedItem.setBuilding_name(building_to_edit.getName());

                            customAdapter.notifyDataSetChanged();


                        }

                    }
                    return true;
                }
            });
        }

    }

    public void editBuidingAlertDialog(final Building building){

        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = inflater.inflate(R.layout.add_new_building_layout, null);
        builder.setView(view);

        /***
         * custom title for alert dialogue
         */
        final View customTitleView = inflater.inflate(R.layout.custom_title, null);
        TextView title = customTitleView.findViewById(R.id.customtitle);
        ImageView img = customTitleView.findViewById(R.id.imageView3);
        title.setText(" Edit Building " + building.getName());
        img.setImageResource(R.drawable.screwdriver);
        builder.setCustomTitle(customTitleView);

        Button btn_position = view.findViewById(R.id.button_position);
        final TextView mark_lat = view.findViewById(R.id.mark_lat);
        final TextView mark_lon = view.findViewById(R.id.mark_lon);
        final EditText building_adress = view.findViewById(R.id.building_address_edit);
        building_adress.clearFocus();
        final EditText building_name = view.findViewById(R.id.buildingname);
        final EditText building_note = view.findViewById(R.id.description);
        final Spinner building_type_spin = view.findViewById(R.id.building_type_spin);
        blocksViewGroup = view.findViewById(R.id.block_container);
        Button add_block_btn = view.findViewById(R.id.add_block_btn);

        ///init data
        btn_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Enable the location layer. Request the location permission if needed.
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);

                    Location loc = mMap.getMyLocation();
                    if (loc != null) {
                        LatLng latLng = new LatLng(loc.getLatitude(), loc
                                .getLongitude());
                        mark_lat.setText("" + latLng.latitude);
                        mark_lon.setText("" + latLng.longitude);
                        building_adress.setText(getCompleteAddressString(latLng.latitude, latLng.longitude));
                    } else {
                        mark_lat.setText("N/A");
                        mark_lon.setText("N/A");
                        building_adress.setText("N/A");
                        Toast.makeText(getApplicationContext(), "***SYSTEM COULD NOT RETRIEVE LOCATION DATA*** " +
                                "\nPossible Causes:" +
                                "\n**GPS is not enabled on your device ==> ENABLE GPS" +
                                "\n**GPS functionnality does not work properly INDOORS ==> Try again OUTDOORS", Toast.LENGTH_LONG).show();
                    }

                } else {
                    // Uncheck the box until the layer has been enabled and request missing permission.
                    PermissionUtils.requestPermission(MBMapsActivity.this, LOCATION_PERMISSION_REQUEST_CODE,
                            Manifest.permission.ACCESS_FINE_LOCATION, false);
                }
            }
        });

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_white_text, building_type_array);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        building_type_spin.setAdapter(dataAdapter);


        mark_lat.setText(building.getLat());
        mark_lon.setText(building.getLon());
        building_adress.setText(building.getAddress());
        building_name.setText(building.getName());
        building_note.setText(building.getNote());


        if(building.getType().equals("MDU")){
            building_type_spin.setSelection(building_type_array.indexOf("MDU"));
        }else  {
            building_type_spin.setSelection(building_type_array.indexOf("SDU"));
        }
        initBlocksTree(blocksViewGroup, building);

        add_block_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewBlockAlert(blocksViewGroup, building);
            }
        });


        builder.setPositiveButton("Save", null);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(building_adress.getText().toString().equals("N/A")) {
                    Toast.makeText(getApplicationContext(), "Building ADDRESS is missing", Toast.LENGTH_LONG).show();
                }else if(mark_lat.getText().toString().equals("N/A")) {
                    Toast.makeText(getApplicationContext(), "Building LATITUDE is missing", Toast.LENGTH_LONG).show();
                }else if(mark_lon.getText().toString().equals("N/A")) {
                    Toast.makeText(getApplicationContext(), "Building LONGITUDE is missing", Toast.LENGTH_LONG).show();
                }else if(building_name.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Building NAME is missing", Toast.LENGTH_LONG).show();
                }else if(building_note.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Building NOTE is missing", Toast.LENGTH_LONG).show();
                }else if(building.getBlocks().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Every BUILDING, must have at least one BLOCK. ", Toast.LENGTH_LONG).show();
                }else if(AppContext.CBLOCK.getFloors().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Every BLOCK, must have at least one FLOOR. ", Toast.LENGTH_LONG).show();
                } else {

                    building.setAddress(building_adress.getText().toString());
                    building.setLat(mark_lat.getText().toString());
                    building.setLon(mark_lon.getText().toString());
                    building.setName(building_name.getText().toString());
                    building.setNote(building_note.getText().toString());
                    String building_type = building_type_spin.getSelectedItem().toString();
                    try {
                        building.setType(building_type);
                    }catch (Exception e){
                        building.setType("MDU");
                    }

                    createSharedPref(buildings_list);

                    selectedItem.setSelected(false);
                    selectedItem.setBuilding_name(building.getName());

                    customAdapter.notifyDataSetChanged();
                    dialog.cancel();
                }
            }
        });
    }

    public void addNewBlockAlert(final ViewGroup blocksview, final Building building) {

        final Block block = new Block();
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Block Name");
        View vv = inflater.inflate(R.layout.add_block_layout, null);
        builder.setView(vv);

        final EditText block_name = vv.findViewById(R.id.block_name_edit);


        builder.setPositiveButton("Save", null);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(block_name.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "EMPTY INPUT IN BLOCK NAME", Toast.LENGTH_LONG).show();
                }else {
                    block.setName(block_name.getText().toString());
                    building.getBlocks().add(block);
                    initBlocksTree(blocksview, building);
                    dialog.cancel();
                }
            }
        });
    }

    public void editBlockAlert(final ViewGroup blocksview, final Building building, final Block block) {

        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Edit Block " + block.getName());
        View vv = inflater.inflate(R.layout.add_block_layout, null);
        builder.setView(vv);

        final EditText block_name = vv.findViewById(R.id.block_name_edit);
        block_name.setText(block.getName());


        builder.setPositiveButton("Save", null);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(block_name.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "EMPTY INPUT IN BLOCK NAME", Toast.LENGTH_LONG).show();
                }else {
                    block.setName(block_name.getText().toString());
                    initBlocksTree(blocksview, building);
                    dialog.cancel();
                }
            }
        });
    }

    public void addNewFloorAlert(final ViewGroup blocksview, final Block block) {

        final Floor floor = new Floor();
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Add New Floor");
        View vv = inflater.inflate(R.layout.add_floor_layout, null);
        builder.setView(vv);

        final EditText floor_name = vv.findViewById(R.id.floor_name_edit);
        final EditText floor_rank = vv.findViewById(R.id.floor_rank_edit);
        final Spinner floor_type_spin = vv.findViewById(R.id.floor_type_spin);

        List<String> floor_type_list = new ArrayList<>();
        floor_type_list.add("Residential");
        floor_type_list.add("Ground Floor");
        floor_type_list.add("Basement");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, floor_type_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        floor_type_spin.setAdapter(dataAdapter);
        floor_type_spin.setSelection(0);

        floor_rank.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                floor_name.setText("floor "+floor_rank.getText().toString());
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();

            }
        });

        builder.setPositiveButton("Save", null);

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (floor_rank.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "EMPTY INPUT IN FLOOR NAME", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        int number = Integer.parseInt(floor_rank.getText().toString());
                        floor.setRank(number);
                        floor.setName(floor_name.getText().toString());
                        String type = floor_type_spin.getSelectedItem().toString();
                        floor.setType(type);
                        block.getFloors().add(floor);
                        initBlocksTree(blocksview, AppContext.CBUILDING);
                        Toast.makeText(getApplicationContext(), "Floor: " + floor.getName() +
                                        " is added to Block: " + block.getName(),
                                Toast.LENGTH_LONG).show();
                        dialog.cancel();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "WRONG INPUT IN FLOOR RANK", Toast.LENGTH_LONG).show();
                        floor_rank.getText().clear();
                        floor_name.getText().clear();
                    }
                }
            }
        });

    }

    public void editFloorAlert(final ViewGroup blocksview, final Floor floor) {

        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Edit Floor " + floor.getName());
        View vv = inflater.inflate(R.layout.add_floor_layout, null);
        builder.setView(vv);

        final EditText floor_name = vv.findViewById(R.id.floor_name_edit);
        floor_name.setText(floor.getName());
        final EditText floor_rank = vv.findViewById(R.id.floor_rank_edit);
        floor_rank.setText(""+floor.getRank());
        final Spinner floor_type_spin = vv.findViewById(R.id.floor_type_spin);

        List<String> floor_type_list = new ArrayList<>();
        floor_type_list.add("Residential");
        floor_type_list.add("Ground Floor");
        floor_type_list.add("Basement");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, floor_type_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        floor_type_spin.setAdapter(dataAdapter);

        floor_rank.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                floor_name.getText().clear();
                floor_name.setText("floor "+floor_rank.getText().toString());
            }
        });

        if(floor.getType().equals("Residential")) {
            floor_type_spin.setSelection(floor_type_list.indexOf("Residential"));
        }else if(floor.getType().equals("Ground Floor")) {
            floor_type_spin.setSelection(floor_type_list.indexOf("Ground Floor"));
        }else {
            floor_type_spin.setSelection(floor_type_list.indexOf("Basement"));
        }

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();

            }
        });

        builder.setPositiveButton("Save", null);

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (floor_name.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "EMPTY INPUT IN FLOOR NAME", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        int number = Integer.parseInt(floor_rank.getText().toString());
                        floor.setRank(number);
                        floor.setName(floor_name.getText().toString());
                        String type = floor_type_spin.getSelectedItem().toString();
                        floor.setType(type);
                        initBlocksTree(blocksview, AppContext.CBUILDING);
                        Toast.makeText(getApplicationContext(), "Floor: " + floor.getName() +
                                        " is succesfully Updated. ",
                                Toast.LENGTH_LONG).show();
                        dialog.cancel();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "WRONG INPUT IN FLOOR RANK", Toast.LENGTH_LONG).show();
                        floor_rank.getText().clear();
                        floor_name.getText().clear();
                    }
                }
            }
        });

    }

    public void addNewAppartementAlert(final ViewGroup blocksview, final Floor floor) {

        final Appartment appartment = new Appartment();
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Add New Appartment");
        View vv = inflater.inflate(R.layout.add_appartment_layout, null);
        builder.setView(vv);

        builder.setPositiveButton("Save", null);

        final Spinner appartment_type_spin = vv.findViewById(R.id.appartment_type_spin);
        final EditText appartment_rank = vv.findViewById(R.id.appartment_rank);
        final EditText appartment_name = vv.findViewById(R.id.appartment_name);
        final Spinner appartment_con_spin = vv.findViewById(R.id.appartment_con_spin);

        List<String> connection_list = new ArrayList<>();
        connection_list.add("P2P");
        connection_list.add("GPON");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, connection_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        appartment_con_spin.setAdapter(dataAdapter);
        appartment_con_spin.setSelection(0);

        appartment_rank.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                appartment_name.setText("appartment "+appartment_rank.getText().toString());
            }
        });

        List<String> type_list = new ArrayList<>();
        type_list.add("RESIDENTIAL");
        type_list.add("BUSINESS");
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, type_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        appartment_type_spin.setAdapter(dataAdapter1);
        appartment_type_spin.setSelection(0);


        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appartment_rank.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "EMPTY INPUT IN APPARTMENT RANK", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        int number = Integer.parseInt(appartment_rank.getText().toString());
                        String con = appartment_con_spin.getSelectedItem().toString();
                        String type = appartment_type_spin.getSelectedItem().toString();
                        appartment.setRank(number);
                        appartment.setType(type);
                        appartment.setConnection(con);
                        appartment.setName(appartment_name.getText().toString());
                        floor.getAppartmens().add(appartment);
                        initBlocksTree(blocksview, AppContext.CBUILDING);
                        Toast.makeText(getApplicationContext(), "Appartment: " + appartment.getRank() +
                                        " is added to Floor: " + floor.getName(),
                                Toast.LENGTH_LONG).show();
                        dialog.cancel();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "WRONG INPUT IN APPARTMENT RANK", Toast.LENGTH_LONG).show();
                        appartment_rank.getText().clear();
                        appartment_name.getText().clear();
                    }

                }
            }
        });

    }

    public void editAppartementAlert(final ViewGroup blocksview, final Appartment appartment) {

        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Edit Appartment " + appartment.getRank());
        View vv = inflater.inflate(R.layout.add_appartment_layout, null);
        builder.setView(vv);

        builder.setPositiveButton("Save", null);

        final Spinner appartment_type_spin = vv.findViewById(R.id.appartment_type_spin);
        final EditText appartment_name = vv.findViewById(R.id.appartment_name);
        appartment_name.setText(appartment.getName());
        final EditText appartment_rank = vv.findViewById(R.id.appartment_rank);
        appartment_rank.setText("" + appartment.getRank());
        final Spinner appartment_con_spin = vv.findViewById(R.id.appartment_con_spin);

        appartment_rank.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                appartment_name.getText().clear();
                appartment_name.setText("appartment "+appartment_rank.getText().toString());
            }
        });
        List<String> connection_list = new ArrayList<>();
        connection_list.add("P2P");
        connection_list.add("GPON");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, connection_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        appartment_con_spin.setAdapter(dataAdapter);

        if(appartment.getType().equals("P2P")) {
            appartment_con_spin.setSelection(connection_list.indexOf("P2P"));
        }else {
            appartment_con_spin.setSelection(connection_list.indexOf("GPON"));
        }

        List<String> type_list = new ArrayList<>();
        type_list.add("RESIDENTIAL");
        type_list.add("BUSINESS");
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, type_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        appartment_type_spin.setAdapter(dataAdapter1);

        if(appartment.getType().equals("RESIDENTIAL")) {
            appartment_con_spin.setSelection(type_list.indexOf("RESIDENTIAL"));
        }else {
            appartment_con_spin.setSelection(type_list.indexOf("BUSINESS"));
        }


        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appartment_rank.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "EMPTY INPUT IN APPARTMENT RANK", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        int number = Integer.parseInt(appartment_rank.getText().toString());
                        String con = appartment_con_spin.getSelectedItem().toString();
                        String type = appartment_type_spin.getSelectedItem().toString();
                        appartment.setRank(number);
                        appartment.setType(type);
                        appartment.setConnection(con);
                        appartment.setName(appartment_name.getText().toString());
                        initBlocksTree(blocksview, AppContext.CBUILDING);
                        Toast.makeText(getApplicationContext(), "Appartment: " + appartment.getRank() +
                                        " is successfully Updated.",
                                Toast.LENGTH_LONG).show();
                        dialog.cancel();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "WRONG INPUT IN APPARTMENT RANK", Toast.LENGTH_LONG).show();
                        appartment_rank.getText().clear();
                    }

                }
            }
        });

    }

    public void initBlocksTree(ViewGroup blocksview, Building tmp) {

        blocksview.removeAllViews();

        TreeNode blocks_root = TreeNode.root();
        buildBlocksTree(tmp, blocks_root);
        block_treeView = new TreeView(blocks_root, this, new NodeViewFactory());
        View view = block_treeView.getView();
        view.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        blocksview.addView(view);
    }

    public void buildBlocksTree(Building building, TreeNode blocks_root) {

        for (Block tmp : building.getBlocks()) {
            TreeNode treeNode = new TreeNode(building, tmp);
            treeNode.setLevel(0);

            for (Floor tmp2 : tmp.getFloors()) {
                TreeNode treeNode1 = new TreeNode(tmp, tmp2);
                treeNode1.setLevel(1);

                for (Appartment tmp3 : tmp2.getAppartmens()) {
                    TreeNode treeNode2 = new TreeNode(tmp2, tmp3);
                    treeNode2.setLevel(2);
                    treeNode1.addChild(treeNode2);
                }

                treeNode.addChild(treeNode1);
            }
            blocks_root.addChild(treeNode);
        }
    }

    public class NodeViewFactory extends BaseNodeViewFactory {

        @Override
        public BaseNodeViewBinder getNodeViewBinder(View view, int level) {
            switch (level) {
                case 0:
                    return new BlockFirstLevelNodeViewBinder(view);
                case 1:
                    return new FloorSecondLevelNodeViewBinder(view);
                case 2:
                    return new AppartmentThirdLevelNodeViewBinder(view);

                default:
                    return null;
            }
        }
    }

    public class BlockFirstLevelNodeViewBinder extends CheckableNodeViewBinder implements View.OnClickListener {
        TextView name;
        ImageView imageView;
        ImageButton add_btn;
        ImageButton delete_btn;
        ImageButton edit_btn;
         Block block;

        public BlockFirstLevelNodeViewBinder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.block_name);
            imageView = itemView.findViewById(R.id.arrow_img);
            add_btn = itemView.findViewById(R.id.add_btn);
            delete_btn = itemView.findViewById(R.id.delete_btn);
            edit_btn = itemView.findViewById(R.id.edit_btn);
        }

        @Override
        public int getCheckableViewId() {
            return 0;
        }

        @Override
        public int getLayoutId() {
            return R.layout.block_item_layout;
        }

        @Override
        public void bindView(final TreeNode treeNode) {

            block = (Block) treeNode.getValue();
            AppContext.CBLOCK = block;
            name.setText(block.getName());
            add_btn.setOnClickListener(this);
            delete_btn.setOnClickListener(this);
            edit_btn.setOnClickListener(this);

            imageView.setRotation(treeNode.isExpanded() ? 90 : 0);

        }

        @Override
        public void onNodeToggled(TreeNode treeNode, boolean expand) {
            if (expand) {
                imageView.animate().rotation(90).setDuration(200).start();
            } else {
                imageView.animate().rotation(0).setDuration(200).start();
            }
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.add_btn){
                addNewFloorAlert(blocksViewGroup, block);
            }else if(view.getId() == R.id.edit_btn){
                editBlockAlert(blocksViewGroup, AppContext.CBUILDING, block);
            }else if(view.getId() == R.id.delete_btn){

                AlertDialog.Builder builder = new AlertDialog.Builder(MBMapsActivity.this) ;
                builder.setMessage("Are you sure you want to remove "+block.getName()+" ?") ;
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        AppContext.CBUILDING.getBlocks().remove(block);
                        initBlocksTree(blocksViewGroup, AppContext.CBUILDING);
                        dialog.cancel();

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });

                AlertDialog dialog = builder.create() ;
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }
    }

    public class FloorSecondLevelNodeViewBinder extends CheckableNodeViewBinder implements View.OnClickListener {

        TextView name;
        TextView rank;
        TextView type;
        ImageView imageView;
        ImageButton add_btn;
        ImageButton delete_btn;
        ImageButton edit_btn;
         Floor floor;
         Block parent_block;


        public FloorSecondLevelNodeViewBinder(View itemView) {

            super(itemView);
            name = itemView.findViewById(R.id.floor_name);
            imageView = itemView.findViewById(R.id.arrow_img);
            add_btn = itemView.findViewById(R.id.add_btn);
            delete_btn = itemView.findViewById(R.id.delete_btn);
            edit_btn = itemView.findViewById(R.id.edit_btn);
            rank = itemView.findViewById(R.id.rank_floor);
            type = itemView.findViewById(R.id.service_floor);

        }

        @Override
        public int getCheckableViewId() {
            return 1;
        }

        @Override
        public int getLayoutId() {
            return R.layout.floor_item_layout;
        }

        @Override
        public void bindView(final TreeNode treeNode) {
             parent_block = (Block) treeNode.getParentT();
            floor = (Floor) treeNode.getValue();
            name.setText(floor.getName());
            rank.setText("" + floor.getRank());
            type.setText(floor.getType());
            imageView.setRotation(treeNode.isExpanded() ? 90 : 0);

            add_btn.setOnClickListener(this);
            edit_btn.setOnClickListener(this);
            delete_btn.setOnClickListener(this);

        }

        @Override
        public void onNodeToggled(TreeNode treeNode, boolean expand) {
            if (expand) {
                imageView.animate().rotation(90).setDuration(200).start();
            } else {
                imageView.animate().rotation(0).setDuration(200).start();
            }
        }

        @Override
        public void onClick(View view) {
            if(view.getId() == R.id.add_btn) {
                addNewAppartementAlert(blocksViewGroup, floor);
            }else if(view.getId() == R.id.edit_btn) {
                editFloorAlert(blocksViewGroup , floor);
            }else if(view.getId() == R.id.delete_btn) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MBMapsActivity.this) ;
                builder.setMessage("Are you sure you want to remove "+floor.getName()+" ?") ;
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        parent_block.getFloors().remove(floor);
                        initBlocksTree(blocksViewGroup, AppContext.CBUILDING);
                        dialog.cancel();

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });

                AlertDialog dialog = builder.create() ;
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }
    }

    public class AppartmentThirdLevelNodeViewBinder extends CheckableNodeViewBinder implements View.OnClickListener {
        TextView name;
        TextView rank;
        TextView connection;
        TextView type;
        ImageButton edit_btn;
        ImageButton delete_btn;
        Appartment appartment;
        Floor parent_floor;


        public AppartmentThirdLevelNodeViewBinder(View itemView) {

            super(itemView);
            name = itemView.findViewById(R.id.appartment_name_);
            rank = itemView.findViewById(R.id.rank);
            connection = itemView.findViewById(R.id.connection);
            type = itemView.findViewById(R.id.appartment_type);
            edit_btn = itemView.findViewById(R.id.edit_btn);
            delete_btn = itemView.findViewById(R.id.delete_btn);

        }

        @Override
        public int getCheckableViewId() {
            return 3;
        }

        @Override
        public int getLayoutId() {
            return R.layout.appartment_item_layout;
        }

        @Override
        public void bindView(final TreeNode treeNode) {
            parent_floor = (Floor) treeNode.getParentT();
            appartment = (Appartment) treeNode.getValue();
            name.setText(appartment.getName());
            type.setText(appartment.getType());
            rank.setText("" + appartment.getRank());
            connection.setText(appartment.getConnection());
            edit_btn.setOnClickListener(this);
            delete_btn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(view.getId() == R.id.edit_btn) {
                editAppartementAlert(blocksViewGroup, appartment);
            }else if(view.getId() == R.id.delete_btn) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MBMapsActivity.this) ;
                builder.setMessage("Are you sure you want to remove Appartment "+appartment.getRank()+" ?") ;
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        parent_floor.getAppartmens().remove(appartment);
                        initBlocksTree(blocksViewGroup, AppContext.CBUILDING);
                        dialog.cancel();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });

                AlertDialog dialog = builder.create() ;
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
                strAdd = "N/A";
            }
        } catch (Exception e) {
            strAdd = "N/A";
            e.printStackTrace();
        }
        return strAdd;
    }

    public void createSharedPref(BuildingsList list) {
        SharedPreferences.Editor editor = getSharedPreferences("WO_MARKED_BUILDINGS", MODE_PRIVATE).edit();
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            String st = objectMapper.writeValueAsString(list);
            editor.putString("buildings", st);
            editor.commit();
        } catch (JsonProcessingException e) {
            editor.apply();
        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

       customAdapter.clear();
       customAdapter.notifyDataSetChanged();

        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        for (Marker marker : markers.keySet()){

            DataModel model = new DataModel();
            model.setSelected(false);
            model.setBuilding_name(marker.getTitle());

            if(bounds.contains(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude))){
                customAdapter.add(model);
                customAdapter.notifyDataSetChanged();
            }else {
                customAdapter.remove(model);
                customAdapter.notifyDataSetChanged();
            }
        }


    }

    public void generateXlsAlert(){

        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View vv = inflater.inflate(R.layout.generate_file_layout, null);
        builder.setView(vv);

        /***
         * custom title for alert dialogue
         */
        final View customTitleView = inflater.inflate(R.layout.custom_title, null);
        TextView title = customTitleView.findViewById(R.id.customtitle);
        ImageView img = customTitleView.findViewById(R.id.imageView3);
        title.setText("Generate XLSX File");
        img.setImageResource(R.drawable.xlsx_img);
        builder.setCustomTitle(customTitleView);


        final EditText zone_name = vv.findViewById(R.id.zone_name);
        zone_name.clearFocus();
        final EditText zone_code = vv.findViewById(R.id.zone_code);
        zone_code.clearFocus();
        ListView listView = vv.findViewById(R.id.container);

        List<String> list = new ArrayList<>();
        for (DataModel tmp : checkedItems){
            list.add(tmp.getBuilding_name());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, list);
        // Assign adapter to ListView
        listView.setAdapter(adapter);


        builder.setPositiveButton("Generate", null);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GenerateXlsx gen = new GenerateXlsx(activity,AppContext.BUILDINGS_LIST, zone_name.getText().toString(), zone_code.getText().toString());
                Toast.makeText(getApplicationContext(), "XLSX File generated Succesfully.", Toast.LENGTH_LONG).show();
                for (DataModel ss : checkedItems) {
                    ss.setSelected(false);
                }
                gen.writeSheet();
                checkedItems.clear(); //remove all items
                AppContext.BUILDINGS_LIST.getBuildings().clear(); //remove all buildings from static object
                customAdapter.notifyDataSetChanged();

                dialog.cancel();

            }
        });
    }

}
